[![coverage report](https://gitlab.com/ModernisingMedicalMicrobiology/rebinner/badges/master/coverage.svg)](https://gitlab.com/ModernisingMedicalMicrobiology/rebinner/commits/master)
[![pipeline status](https://gitlab.com/ModernisingMedicalMicrobiology/rebinner/badges/master/pipeline.svg)](https://gitlab.com/ModernisingMedicalMicrobiology/rebinner/commits/master)


# rebinner

Rearrange reads in multi fast5 files to bins based on input list, fastq or bam file.

## Usage

The command line options are as follows:

```
usage: rebinner.py [-h] [-f5s F5_FILES [F5_FILES ...]]
                   [-w WANTLIST [WANTLIST ...]]
                   [-fq FASTQ_FILES [FASTQ_FILES ...]]
                   [-fa FASTA_FILES [FASTA_FILES ...]]
                   [-bam BAM_FILES [BAM_FILES ...]] [-o OUT_DIRECTORY]
                   [-b BATCH_SIZE] [-s SUMMARY_FILE]

Rearrange reads in multi fast5 files to bins based on input list, fastq or bam
file.

optional arguments:
  -h, --help            show this help message and exit
  -f5s F5_FILES [F5_FILES ...], --f5_files F5_FILES [F5_FILES ...]
                        Multi fast5 file(s)
  -w WANTLIST [WANTLIST ...], --wantList WANTLIST [WANTLIST ...]
                        want list of read names
  -fq FASTQ_FILES [FASTQ_FILES ...], --fastq_files FASTQ_FILES [FASTQ_FILES ...]
                        Fastq file(s)
  -fa FASTA_FILES [FASTA_FILES ...], --fasta_files FASTA_FILES [FASTA_FILES ...]
                        Fasta file(s)
  -bam BAM_FILES [BAM_FILES ...], --bam_files BAM_FILES [BAM_FILES ...]
                        bam file(s)
  -o OUT_DIRECTORY, --out_directory OUT_DIRECTORY
                        ouput directory for multi fast5 files,
                        default="binned_fast5s"
  -b BATCH_SIZE, --batch_size BATCH_SIZE
                        number of reads per output fast5 file bins,
                        default=8000
  -s SUMMARY_FILE, --summary_file SUMMARY_FILE
                        Output summary file for bins,
                        default="binned_summary.txt"
```

To bin based on fastq files, for example a number of fastq files already demultiplexed by porechop, this command can be used.

```
rebinner.py -fq basecalled_fastq/* -f5s fast5_pass/*
```

This will produced an output folder with multiple fast5 files per barcode. The new fast file files will have the same base name as the input fastq or bam files, and a prefix for batch number. This number increases with the number of reads binned, and is not relative to the input fast5 file batch numbers. There will also be a summary text file that is intened as a lookup table similar to the sequencing_summary files produced by guppy or albacore.
