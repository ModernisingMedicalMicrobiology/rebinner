#!/usr/bin/env python3
from setuptools import setup, find_packages
import glob

install_requires = [
        'pysam',
        'pandas',
        'Bio',
        'tqdm',
        'h5py'
        ]

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
     name='rebinner',
     version='0.1',
     description='Rearrange reads in multi fast5 files to bins based on input list, fastq or bam file.',
     author=['Nick Sanderson'],
     author_email='nicholas.sanderson@ndm.ox.ac.uk',
     scripts=['rebinner/rebinner.py'],
     packages=find_packages('rebinner'),
     package_dir = {'': 'rebinner'},
     include_package_data=True,
     long_description=long_description,
     long_description_content_type="text/markdown",
     license='MIT',
     url='https://gitlab.com/ModernisingMedicalMicrobiology/rebinner',
     test_suite='nose.collector',
     tests_require=['nose'],
     install_requires=install_requires
)

