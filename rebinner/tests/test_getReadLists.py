import unittest
import os,shutil
from rebinner.rebinner import reBinner 
from argparse import ArgumentParser, SUPPRESS


modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')
fastqs=os.path.join(data_dir, 'fastqs')

# args
parser = ArgumentParser(description='Rearrange reads in multi fast5 files to bins based on input list, fastq or bam file.')
parser.add_argument('-f5s', '--f5_files', required=False,nargs='+',
                    help='Multi fast5 file(s)')
parser.add_argument('-w', '--wantList', required=False,nargs='+',
                    help='want list of read names')
parser.add_argument('-fq', '--fastq_files', required=False,nargs='+',
                    help='Fastq file(s)')
parser.add_argument('-fa', '--fasta_files', required=False,nargs='+',
                    help='Fasta file(s)')
parser.add_argument('-bam', '--bam_files', required=False,nargs='+',
                    help='bam file(s)')
parser.add_argument('-o', '--out_directory', required=False,default='binned_fast5s',
                    help='ouput directory for multi fast5 files, default="binned_fast5s"')
parser.add_argument('-b', '--batch_size', required=False,default=8000,
                    help='number of reads per output fast5 file bins, default=8000')
parser.add_argument('-s', '--summary_file', required=False,default='binned_summary.txt',
                    help='Output summary file for bins, default="binned_summary.txt"')

opts, unknown_args = parser.parse_known_args()
kditc=vars(opts)

class testGetLists(unittest.TestCase):

    def setUp(self):
        self.r=reBinner(**kditc)

    def testGetFqBins(self):
        self.r.fastq_files=[]
        self.r.fastq_files.append(os.path.join(fastqs, 'BC05.fastq.gz'))
        self.r.fastq_files.append(os.path.join(fastqs, 'BC06.fastq.gz'))
        self.r.getFqBins()
        print(self.r.binDict)
        print(self.r.fileDict)
        assert len(self.r.binDict['BC05']) == 6
        assert len(self.r.binDict['BC06']) == 13
        assert self.r.fileDict['read_35c04a77-bfe1-453e-b680-d77f81648c81'] == 'BC05.fastq.gz'
        assert self.r.fileDict['read_267acb8a-4069-42c5-b37d-dfd18ec6c094'] == 'BC06.fastq.gz'

 


