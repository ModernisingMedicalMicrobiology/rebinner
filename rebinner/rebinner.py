#!/usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
import gzip
import os
import pysam
import pandas as pd
from Bio import SeqIO
from tqdm import tqdm
import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=FutureWarning)
    import h5py

def invert_dict(d): 
    inverse = dict() 
    for key in d: 
        # Go through the list that is saved in the dict:
        for item in d[key]:
            # Check if in the inverted dict the key exists
            if item not in inverse: 
                # If not create a new list
                inverse[item] = key 
            else: 
                inverse[item]=key
    return inverse

class reBinner:
    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

        self.binDict={}
        self.summary=[]
        self.fileDict={}

        if self.wantList != None:
            self.binDict[self.outBin]=self.wantList

        if self.fastq_files != None:
            self.getFqBins()

        if self.fasta_files != None:
            self.getFaBins()

        if self.bam_files != None:
            self.getBamBins()

    def run(self):
        if len(self.binDict)>0:
            print('Bin Name','Number of reads',sep='\t')
            for b in self.binDict:
                print(b,len(self.binDict[b]),sep='\t')

            self.binDictInverse=invert_dict(self.binDict)

            if not os.path.exists(self.out_directory):
                os.makedirs(self.out_directory)

            self.processBatch()

            df=pd.DataFrame(self.summary)
            df.to_csv(self.summary_file,index=False,sep='\t')
        else:
            print('No bins?')

    def getFqBins(self):
        for fq in self.fastq_files:
            b=os.path.basename(fq).split('.')[0]
            if fq.endswith('gz'):
                mo=gzip.open
            else:
                mo=open
            for seq in SeqIO.parse(mo(fq,'rt'),'fastq'):
                self.binDict.setdefault(b,[]).append('read_{0}'.format(seq.id))
                self.fileDict.setdefault('read_{0}'.format(seq.id),fq.split('/')[-1])

    def getFaBins(self):
        for fa in self.fasta_files:
            b=os.path.basename(fa).split('.')[0]
            if fa.endswith('gz'):
                mo=gzip.open
            else:
                mo=open
            for seq in SeqIO.parse(mo(fa,'rt'),'fasta'):
                self.binDict.setdefault(b,[]).append('read_{0}'.format(seq.id))
                self.fileDict.setdefault('read_{0}'.format(seq.id),fa.split('/')[-1])

    def getBamBins(self):
        for bam in self.bam_files:
            b=os.path.basename(bam).split('.')[0]
            samfile = pysam.AlignmentFile(bam, "rb")
            for read in samfile.fetch():
                self.binDict.setdefault(b,[]).append('read_{0}'.format(read.query_name))
                self.fileDict.setdefault('read_{0}'.format(read.query_name),bam.split('/')[-1])

    def processBatch(self):
        dr,fr=0,0
        self.outF5s={}
        for b in self.binDict:
            self.outF5s[b]={'File':h5py.File('{0}/{1}_0.fast5'.format(self.out_directory,b),'w'),
                    'batchN':0,'reads':0}

        for f5 in tqdm(self.f5_files):
            with h5py.File(f5,'r') as f:
                for read in list(f.keys()):
                    if read in self.binDictInverse:
                        rd=f[read]
                        b=self.binDictInverse[read]
                        f.copy(rd, self.outF5s[b]['File']['/'])

                        sumd={'filename_fastq':self.fileDict[read],
                                'filename_fast5':'{1}_{2}.fast5'.format(self.out_directory,
                                b,
                               str(self.outF5s[b]['batchN'])),
                                'read_id':read.replace('read_','')}

                        self.summary.append(sumd)

                        self.outF5s[b]['reads']+=1
                        if self.outF5s[b]['reads'] == self.batch_size:
                            self.outF5s[b]['reads']=0
                            self.outF5s[b]['batchN']+=1
                            self.outF5s[b]['File']=h5py.File('{0}/{1}_{2}.fast5'.format(self.out_directory,
                                b,
                                str(self.outF5s[b]['batchN'])),'w')

                        dr+=1
                    else:
                        fr+=1
        print("Binned reads: {0}\nNeglected reads: {1}\n".format(dr,fr))

def bf5s_args(parser):
    parser.add_argument('-f5s', '--f5_files', required=False,nargs='+',
                        help='Multi fast5 file(s)')
    parser.add_argument('-w', '--wantList', required=False,nargs='+',
                        help='want list of read names')
    parser.add_argument('-fq', '--fastq_files', required=False,nargs='+',
                        help='Fastq file(s)')
    parser.add_argument('-fa', '--fasta_files', required=False,nargs='+',
                        help='Fasta file(s)')
    parser.add_argument('-bam', '--bam_files', required=False,nargs='+',
                        help='bam file(s)')
    parser.add_argument('-o', '--out_directory', required=False,default='binned_fast5s',
                        help='ouput directory for multi fast5 files, default="binned_fast5s"')
    parser.add_argument('-b', '--batch_size', required=False,default=8000,
                        help='number of reads per output fast5 file bins, default=8000')
    parser.add_argument('-s', '--summary_file', required=False,default='binned_summary.txt',
                        help='Output summary file for bins, default="binned_summary.txt"')
    return parser

def run(opts):
    kditc=vars(opts)
    r=reBinner(**kditc)
    r.run()

if __name__ == '__main__':
    parser = ArgumentParser(description='Rearrange reads in multi fast5 files to bins based on input list, fastq or bam file.')
    parser = bf5s_args(parser)
    opts, unknown_args = parser.parse_known_args()
    run(opts)
